<?php 
include_once('cCarrera.php');

$carrera = new carrera();
$lista=$carrera->read();
?>

<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CRUD con PHP usando Programación Orientada a Objetos</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/custom.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Listado de  <b>Carreras</b></h2></div>
                    <div class="col-sm-4">
                        <a href="index.php" class="btn btn-info add-new"><i class="fa fa-plus"></i> Regresar</a>
                    </div>
                </div>
            </div>
            <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-4 ">
                        <a href="createcarrera.php" class="btn btn-info add-new"><i class="fa fa-plus"></i> Agregar</a>
                    </div>
                </div>
            </div>
            
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Codigo Carrea</th>
                        <th>Nombre Carrera</th>
                        <th>Titulo</th>
                        <th>Acciones</th>
                    
                    </tr>
                </thead>
                 
                <tbody>   
                <?php 
while ($row=mysqli_fetch_object($lista)){
$codCarrera=$row->codCarrera;
$nombreCarrera=$row->nombreCarrera;
$titulo=$row->titulo;
?>
<tr>
<td><?php echo $codCarrera;?></td>
<td><?php echo $nombreCarrera;?></td>
<td><?php echo $titulo;?></td>

<td>
<a href="updateCarrera.php?codCarrera=<?php echo $codCarrera;?>" class="edit" title="Editar" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
<a href="deletecarrera.php?codCarrera=<?php echo $codCarrera;?>" class="delete" title="Eliminar" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
</td>
</tr>	
<?php
}
?> 
                          
                </tbody>
            </table>
        </div>
    </div>   

    

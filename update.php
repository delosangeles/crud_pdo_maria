<?php
	if (isset($_GET['codAlumno'])){
		$id=intval($_GET['codAlumno']);
	} else {
		header("location:index.php");
    }
    
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CRUD con PHP usando Programación Orientada a Objetos</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/custom.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Editar <b>Alumnos</b></h2></div>
                    <div class="col-sm-4">
                        <a href="index.php" class="btn btn-info add-new"><i class="fa fa-arrow-left"></i> Regresar</a>
                    </div>
                </div>
            </div>
            <?php
				
				include ("Conexion.php");
				$alumnos= new Database();
				
				if(isset($_POST) && !empty($_POST)){
					$codAlumno = $alumnos->sanitize($_POST['codAlumno']);
					$nombreCompleto = $alumnos->sanitize($_POST['nombreCompleto']);
					$tipoDocumento = $alumnos->sanitize($_POST['tipoDocumento']);
				    $numeroDocumento = $alumnos->sanitize($_POST['numeroDocumento']);
					$sexo= $alumnos->sanitize($_POST['sexo']);
					$correo = $alumnos->sanitize($_POST['correo']);
					$numeroTelefono = $alumnos->sanitize($_POST['numeroTelefono']);
					$codCarrera = $alumnos->sanitize($_POST['codCarrera']);
					$direccion = $alumnos->sanitize($_POST['direccion']);

					$id_alum=intval($_POST['id_alum']);
					$res = $alumnos->update($nombreCompleto, $tipoDocumento, $numeroDocumento, $sexo,$correo,$numeroTelefono,$codCarrera,
					$direccion,$codAlumno,$id_alum);


				

					if($res){
						$message= "Datos actualizados con éxito";
						$class="alert alert-success";
						
					}else{
						$message="No se pudieron actualizar los datos";
						$class="alert alert-danger";
					}
					
					?>
				<div class="<?php echo $class?>">
				  <?php echo $message;?>
				</div>	
					<?php
				}
				$datos_alum=$alumnos->single_record($id);
			?>
			<div class="row">
				<form method="post">
				<div class="col-md-6">
					<label>CodAlumno:</label>
					<input type="text" name="codAlumno" id="codAlumno" class='form-control' maxlength="100" required  value="<?php echo $datos_alum->codAlumno;?>">
					<input type="hidden" name="id_alum" id="id_alum" class='form-control' maxlength="100"   value="<?php echo $datos_alum->id;?>">
				</div>
				<div class="col-md-6">
					<label>NombreCompleto:</label>
					<input type="text" name="nombreCompleto" id="nombreCompleto" class='form-control' maxlength="100" required  value="<?php echo $datos_alum->nombreCompleto;?>">
				</div>
				<div class="col-md-12">
					<label>TipoDocumento:</label>
					<textarea  name="tipoDocumento" id="tipoDocumento" class='form-control' maxlength="255" required><?php echo $datos_alum->tipoDocumento;?></textarea>
				</div>
				<div class="col-md-6">
					<label>NumeroDocumento:</label>
					<input type="text" name="numeroDocumento" id="numeroDocumento" class='form-control' maxlength="15" required value="<?php echo $datos_alum->numeroDocumento;?>">
				</div>
				<div class="col-md-6">
					<label>Sexo:</label>
					<input type="text" name="sexo" id="sexo" class='form-control' maxlength="15" required value="<?php echo $datos_alum->sexo;?>">
				</div>
				<div class="col-md-6">
					<label>Correo:</label>
					<input type="email" name="correo" id="correo" class='form-control' maxlength="64" required value="<?php echo $datos_alum->correo;?>">
				
				</div>
				<div class="col-md-6">
					<label>NumeroTelefono:</label>
					<input type="text" name="numeroTelefono" id="numeroTelefono" class='form-control' maxlength="15" required value="<?php echo $datos_alum->numeroTelefono;?>">
				</div>
				<div class="col-md-6">
					<label>CodCarrera:</label>
					<input type="text" name="codCarrera" id="codCarrera" class='form-control' maxlength="15" required value="<?php echo $datos_alum->codCarrera;?>">
				</div>
				<div class="col-md-6">
					<label>Direccion:</label>
					<input type="text" name="direccion" id="direccion" class='form-control' maxlength="15" required value="<?php echo $datos_alum->direccion;?>">
				</div>
				
				<div class="col-md-12 pull-right">
				<hr>
					<button type="submit" class="btn btn-success">Actualizar datos</button>
				</div>
				</form>
			</div>
        </div>
    </div>     
</body>
</html>